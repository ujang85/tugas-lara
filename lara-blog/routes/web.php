<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|



Route::get('/', function () {
    return view('welcome');
}); 
*/
Route::get('/', [IndexController::class, 'utama']);
Route::get('/biodata', [FormController::class, 'bio'])->name('bio');

Route::get('/master', function(){
    return view('layout.master');
});
Route::get('/data-table', function(){
    return view('page.data-table');
});
Route::get('/table', function(){
    return view('page.table');
});

Route::get('/cast', [CastController::class, 'index']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/create', [CastController::class, 'create']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);


