<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast'=>$cast]);
        
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.detail', ['cast'=>$cast]);
        
    }

    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'umur' => 'required',
            'nama' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            'umur' => $request['umur'],
            'nama' => $request['nama'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required',
            'nama' => 'required',
            'bio' => 'required',
        ]);
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'umur' => $request['umur'],
                'nama' => $request['nama'],
                'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.edit', ['cast'=>$cast]);
        
    }
}
