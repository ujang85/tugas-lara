@extends('layout.master')
@section('title')
<h3>Halaman Data Cast</h3>
@endsection
@section('sub-title')
<h3>Sub Halaman Index Data Cast</h3>
@endsection


@section('content')
<a href="/cast/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No.</th>
                <th scope="col">Umur</th>
                <th scope="col">Nama</th>
                <th scope="col">Bio</th>
                <th scope="col" style="display: inline">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->bio}}</td>
                        
                        <td>
                            <a href="/cast/{{$value->id}}" class="btn btn-info">Detail</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>

                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection