@extends('layout.master')
@section('title')
<h3>Halaman Data Cast</h3>
@endsection
@section('sub-title')
<h3>Sub Halaman Tambah Data Cast</h3>
@endsection

@section('content')
<form action="/cast" method="post">
@csrf
<div class="form-group">
    <label>nama</label>
    <input type="text" class="form-control" name="nama">
    @error('nama')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur">
    @error('umur')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Biodata</label>
    <input type="text" class="form-control" name="bio">
    @error('bio')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
@endsection