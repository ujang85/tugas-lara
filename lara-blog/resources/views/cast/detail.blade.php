@extends('layout.master')
@section('title')
<h3>Halaman Data Cast</h3>
@endsection
@section('sub-title')
<h3>Sub Halaman Detail Data </h3>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detail Cast
                    </div>

                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $cast->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Umur</td>
                                    <td>{{ $cast->umur }}</td>
                                </tr>
                                <tr>
                                    <td>Biodata</td>
                                    <td>{{ $cast->bio }}</td>
                                </tr>
                            </tbody>
                        </table>

                        </form>
                        <a href="/cast" class="btn btn-secondary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
