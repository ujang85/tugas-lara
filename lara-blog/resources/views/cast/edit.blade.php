@extends('layout.master')
@section('title')
<h3>Halaman Data Cast</h3>
@endsection
@section('sub-title')
<h3>Sub Halaman Edit Data Cast</h3>
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
@csrf
@method('PUT')
<div class="form-group">
    <label>nama</label>
    <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    @error('nama')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
    @error('umur')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Biodata</label>
    <input type="text" class="form-control" name="bio" value="{{$cast->bio}}">
    @error('bio')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
@endsection